<?php

namespace Database\Seeders;

use App\Models\School;
use Illuminate\Database\Seeder;

class SchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        School::create(['name' => 'U.E.C. Mariano Picon Salas']);
        School::create(['name' => 'U.E.C. Caura']);
        School::create(['name' => 'U.E.C. Nuestra Señora de Lourdes']);
        School::create(['name' => 'U.E.C. Andres Bello']);
    }
}
