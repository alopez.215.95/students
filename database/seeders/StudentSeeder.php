<?php

namespace Database\Seeders;

use App\Models\Student;
use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::create(['name' => 'Angel', 'lastname' => 'Lopez', 'age' => 25, 'school_id' => 1]);
        Student::create(['name' => 'Alfonso', 'lastname' => 'Perez', 'age' => 20, 'school_id' => 2]);
        Student::create(['name' => 'Pablo', 'lastname' => 'Gallardo', 'age' => 30, 'school_id' => 3]);
        Student::create(['name' => 'Otto', 'lastname' => 'Malave', 'age' => 17, 'school_id' => 4]);
    }
}
