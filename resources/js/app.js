import Vue from 'vue';
require('./bootstrap');
import router from './router';
import store from './store'
import App from './components/App';
store.dispatch('getUser');

import VueAxios from 'vue-axios';
import axios from 'axios';

Vue.use(router);
Vue.use(store);
Vue.use(VueAxios, axios);

import { LayoutPlugin } from 'bootstrap-vue'
Vue.use(LayoutPlugin)
import { NavbarPlugin } from 'bootstrap-vue'
Vue.use(NavbarPlugin)
import { CardPlugin } from 'bootstrap-vue'
Vue.use(CardPlugin)
import { FormPlugin } from 'bootstrap-vue'
Vue.use(FormPlugin)
import { FormGroupPlugin } from 'bootstrap-vue'
Vue.use(FormGroupPlugin)
import { FormInputPlugin } from 'bootstrap-vue'
Vue.use(FormInputPlugin)
import { ButtonPlugin } from 'bootstrap-vue'
Vue.use(ButtonPlugin)
import { TablePlugin } from 'bootstrap-vue'
Vue.use(TablePlugin)
import { ModalPlugin } from 'bootstrap-vue'
Vue.use(ModalPlugin)
import { InputGroupPlugin } from 'bootstrap-vue'
Vue.use(InputGroupPlugin)
import { IconsPlugin } from 'bootstrap-vue'
Vue.use(IconsPlugin)

router.beforeEach((to, from, next) => {
    console.log('meta='+to.meta.requiresAuth)
    console.log('auth='+store.state.auth)
    if (to.meta.requiresAuth)
        if (!store.state.auth)
            setTimeout(function() {
                if(!store.state.auth) next({ name: 'login' })
                else next()
            }, 1700);
        else next()
    else next()
});

const app = new Vue({
    el: '#app',
    components: {
        App
    },
    router,
    store,
});
