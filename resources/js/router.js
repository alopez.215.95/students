import Vue from 'vue';
import Router from 'vue-router';
import HomeComponent from './components/HomeComponent';
import LoginComponent from './components/LoginComponent';
import StudentComponent from './components/StudentComponent';
import NotFound from './components/NotFound';
import SchoolComponent from './components/SchoolComponent';

Vue.use(Router);

export default new Router({
    mode: 'history',
    linkActiveClass: 'is-active',
    routes: [
        { path: '/login', name: 'login', component: LoginComponent, meta: {requiresAuth: false} },
        { path: '/', redirect: '/home', meta: {requiresAuth: false} },
        { path: '/home', name: 'home', component: HomeComponent, meta: {requiresAuth: false} },
        { path: '/student', name: 'student', component: StudentComponent, meta: {requiresAuth: true} },
        { path: '/school', name: 'school', component: SchoolComponent, meta: {requiresAuth: true} },
        { path: '/404', name: '404', component: NotFound, meta: {requiresAuth: false} },
        { path: '*', redirect: '/404', meta: {requiresAuth: false} },
    ]
});
