import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: null,
        auth: false,
    },
    mutations: {
        SET_USER(state, user) {
            state.user = user;
            state.auth = Boolean(user);
        }
    },
    actions: {
        logout({ dispatch }) {
            axios.post('http://students.test/logout').then(response => {
                dispatch("getUser")
            });
        },
        async login({ dispatch },credentials) {
            axios.get('http://students.test/login/sanctum/csrf-cookie').then(response => {
                axios.post('http://students.test/login', credentials).then(response => {
                    dispatch("getUser");
                });
            });
        },
        async getUser({ commit }) {
            await axios.get('http://students.test/api/user')
            .then(response => {
                commit('SET_USER', response.data);
            })
            .catch(() => {
                commit('SET_USER', null);
            });
        }
    },
    modules: {},
});
