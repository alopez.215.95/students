Pasos para la instalacion en el entorno de laragon

1.- Abrir terminal donde desea descargar el proyecto.

2.- Clonar proyecto ejecutando git clone git@gitlab.com:alopez.215.95/students.git

3.- Entrar al directorio raiz del proyecto.

4.- Ejecutar el comando 'composer install' en su terminal.

5.- Crear el archivo .env a partir de .env.example.

5.1.- EN LARAGON: Asignar las siguientes variables con sus valores en el .env SANCTUM_STATEFUL_DOMAINS=students.test y SESSION_DOMAIN=students.test

5.2.- OTRO SERVER: Asignar las siguientes variables con sus valores en el .env SANCTUM_STATEFUL_DOMAINS=localhost:8000 y SESSION_DOMAIN=localhost:8000

6.- Crear base de datos en mysql con el nombre 'students'.

7.- Ejecutar el comando 'php artisan key:generate' en su terminal.

8.- Ejecutar el comando 'npm install' en su terminal.

9.- Ejecutar el comando 'php artisan migrate --seed' en su terminal.

10.- Abrir una segunda terminal el directorio raiz del proyecto y ejecutar 'npm run dev' o 'npm run watch'.

11.- Luego de los pasos anteriormente descritos ejecutar 'php artisan serve' en caso de laragon no es necesario.

12.- Las credenciales para iniciar sesion son las siguientes:

    User: admin@admin.com
    Pass: admin
