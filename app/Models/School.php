<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'schools';

    public $timestamps = true;

    public $fillable = [
        'name'
    ];

    public function students()
    {
        return $this->hasMany(Student::class, 'school_id');
    }

}
