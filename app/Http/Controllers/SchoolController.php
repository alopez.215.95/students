<?php

namespace App\Http\Controllers;

use App\Http\Requests\SchoolRequest;
use App\Models\School;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schools = School::with('students')->get()->toArray();
        return response()->json(array_reverse($schools));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SchoolRequest $request)
    {
        $school = new School($request->all());
        $school->save();

        $newSchool = School::with('students')->where('id', $school->id)->get()->first()->toArray();

        return response()->json(['school' => $newSchool, 'message' => 'La escuela se guardo exitosamente.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\Response
     */
    public function show(School $school)
    {
        return response()->json($school);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\Response
     */
    public function update(SchoolRequest $request, School $school)
    {
        $school->update($request->all());

        $editSchool = School::with('students')->where('id', $school->id)->get()->first()->toArray();

        return response()->json(['school' => $editSchool, 'message' => 'La escuela se actualizo exitosamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\Response
     */
    public function destroy(School $school)
    {
        $school->delete();
        return response()->json(['message' => 'La escuela se elimino exitosamente.']);
    }
}
