<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:30',
            'lastname' => 'required|min:3|max:30',
            'age' => 'required|numeric',
            'school_id' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'El nombre del estudiante es requerido.',
            'name.min' => 'El nombre del estudiante debe ser minimo de 3 caracteres.',
            'name.max' => 'El nombre del estudiante debe ser maximo de 150 caracteres.',
            'lastname.required' => 'El apellido del estudiante es requerido.',
            'lastname.min' => 'El apellido del estudiante debe ser minimo de 3 caracteres.',
            'lastname.max' => 'El apellido del estudiante debe ser maximo de 150 caracteres.',
            'age.required' => 'La edad del estudiante es requerida.',
            'age.numeric' => 'La edad del estudiante debe ser un numero.',
            'school_id.required' => 'La escuela del estudiante es requerida.',
        ];
    }
}
