<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SchoolRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:schools|max:150',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'El nombre de la escuela es requerido.',
            'name.unique' => 'El nombre de la escuela ya ha sido registrado ingrese otro.',
            'name.max' => 'El nombre de la escuela debe ser maximo de 150 caracteres.',
        ];
    }
}
